package single.touch.android.vogella.com.touchapp;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
//import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

public class SingleTouchEventView extends View {
    private Paint paint = new Paint();
    private Paint paint2 = new Paint();
    //private Path path = new Path();
    private float pWidth;
    float [] hlines = new float[56];
    float [] vlines = new float[56];
    Cells[][] grid = null;

    public SingleTouchEventView(Context context, AttributeSet attrs) {
        super(context, attrs);

        paint.setAntiAlias(true);
        paint.setStrokeWidth(6f);
        paint.setColor(Color.BLACK);
        paint2.setColor(Color.RED);
       // paint.setStyle(Paint.Style.STROKE);
        //paint.setStrokeJoin(Paint.Join.ROUND);
    }

    public void make_lines(){
        for (int i = 0; i<14;i++)
        {
            hlines[4*i] = 0;//start at the left side of the screen
            hlines[4*i+1] = (pWidth*(i))/13;//this is the y value
            hlines[4*i+2] = pWidth;//end at the right side of the screen
            hlines[4*i+3] = (pWidth*(i))/13;//this is the y value aas well

            vlines[4*i] = (pWidth*(i))/13;//this is the x value
            vlines[4*i+1] = 0;
            vlines[4*i+2] = (pWidth*(i))/13;//as well as thhhis
            vlines[4*i+3] = pWidth;
        }
    }

    public void make_cells() {
        grid = new Cells[13][13];
        Log.d("o", "make cells");
        float k, l;
        for (int i = 0; i < 13; i++) {
            for (int j = 0; j < 13; j++) {
                k = ((vlines[4 * (j + 1)] - vlines[4 * j]) / 2) + vlines[4 * j];
                l = ((hlines[4 * (i + 1) + 1] - hlines[4 * i + 1]) / 2) + hlines[4 * i + 1];
                grid[i][j] = new Cells(i, j, k, l);
            }
        }
    }

    public void draw_circles(Canvas canvas) {


        for (int i = 0; i < 13; i++) {
            for (int j = 0; j < 13; j++) {

                if((grid[i][j]).this_life_status){
                canvas.drawCircle((grid[i][j]).x_center_point,
                        (grid[i][j]).y_center_point, 25, paint2);}
            }

        }
    }



    //this method shouuld map a touch event to  Cells object, then perhaps toggle the
    public void map_touch_event(float x,float y)

    {
        int i= 0, j= 0;
        while(x > vlines[4*j]){j++;}
        while(y > hlines[4*i+1]){i++;}
        //grid [i-1][j-1] will be the cell to toggle
        (grid[i-1][j-1]).toggle_cell();
    }



    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        pWidth = MeasureSpec.getSize(widthMeasureSpec);
        //int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
       // pWidth = parentWidth;
        //pHeight = parentHeight;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //Log.d("makelines","mmakestuff");
        make_lines();
        if(grid == null) {
            make_cells();
        }
        canvas.drawColor(Color.WHITE);
        canvas.drawLines(hlines, paint);
        canvas.drawLines(vlines, paint);
        draw_circles(canvas);

        //canvas.drawPath(path, paint);
    }

    public int check_neighbors(int a,int b){
        int x =0;

        if(a<12 && b<12 && (grid[a+1][b+1]).this_life_status) x++; //southe
        if(b<12 && (grid[a][b+1]).this_life_status) x++;//south
        if(a>0 && b<12 && (grid[a-1][b+1]).this_life_status) x++;//southw
        if(a>0 && (grid[a-1][b]).this_life_status) x++;//west
        if(b>0 && a>0 && (grid[a-1][b-1]).this_life_status) x++;//northw
        if(b>0 && (grid[a][b-1]).this_life_status) x++;//north
        if(a<12 && b>0 && (grid[a+1][b-1]).this_life_status) x++;//northe
        if(a<12 && (grid[a+1][b]).this_life_status) x++;//east


    return x;

    }

    public void update_cells(){

        int live_neighbors;

        for (int i = 0; i < 13; i++) {
            for (int j = 0; j < 13; j++) {
                //Log.d("thingy","Grid["+i+"]["+j+"]");
                live_neighbors = check_neighbors(i,j);
                //if our cell is currently alive then check some more stuff
                if ((grid[i][j]).this_life_status)
                {
                    Log.d("thongy","Grid["+i+"]["+j+"] has"+live_neighbors+"neighbors");
                    //celll dies due to overpoppulation
                    if(live_neighbors>3)
                    {
                        (grid[i][j]).next_life_status = false;
                    }
                    //cell dies if it gets lonely
                    else if(live_neighbors<2)
                    {
                        (grid[i][j]).next_life_status = false;
                    }
                    else if(live_neighbors==2 ||live_neighbors==3 )(grid[i][j]).next_life_status = true;
                }

                //this is for a currently dead cell that will be born next cycle
                if((grid[i][j]).this_life_status == false && live_neighbors == 3){
                    (grid[i][j]).next_life_status = true;
                }

            }

        }


        for (int i = 0; i < 13; i++) {
            for (int j = 0; j < 13; j++) {
                (grid[i][j]).this_life_status = (grid[i][j]).next_life_status;
            }
        }

    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_UP) {
            Log.d("IM here", "inside touche event");
            float eventX = event.getX();
            float eventY = event.getY();

            Log.d("k", String.valueOf(eventX));
            //i need to map the location of the event to a cell
            map_touch_event(eventX,eventY);

            invalidate();
        }


        return true;
    }


}