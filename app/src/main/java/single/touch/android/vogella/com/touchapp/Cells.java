package single.touch.android.vogella.com.touchapp;

import android.util.Log;
import android.util.StringBuilderPrinter;

import java.util.HashMap;

/**
 * Created by Hugo Quiroz on 2/20/2017.
 */

public class Cells {

    int X;
    int Y;
    float x_center_point;
    float y_center_point;
    boolean this_life_status = false;
    boolean next_life_status = false;
    boolean checked = false;
 //   boolean north, northe, east, southe, south, southw, west, northw;

    public Cells(int a, int b, float c, float d){
        X = a;
        Y = b;
        x_center_point =c;
        y_center_point = d;
    }


    public void toggle_cell()//this method toggles the life status of a cell
    {
        Log.d("f", String.valueOf(this_life_status));
        if(this_life_status==true){
            this_life_status = false;
        }
        else{
            this_life_status = true;
        }
        Log.d("j", String.valueOf(this_life_status));
    }


    }
