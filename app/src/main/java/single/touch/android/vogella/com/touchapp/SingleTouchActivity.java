package single.touch.android.vogella.com.touchapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.content.DialogInterface;


public class SingleTouchActivity extends Activity {

    Button reset_button;
    Button next_button;
    SingleTouchEventView mah_grid;
    final Context context = this;



    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_touch);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);



        // GRid stuff
        mah_grid = (SingleTouchEventView) findViewById(R.id.mah_grid);


        // Button stuff
        reset_button = (Button) findViewById(R.id.reset_button);
        next_button = (Button) findViewById(R.id.next_button);







        reset_button.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            context);

                    // set title
                    alertDialogBuilder.setTitle("Reset Grid");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Click yes to reset!")
                            .setCancelable(false)
                            .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    mah_grid.make_cells();
                                    mah_grid.invalidate();
                                }
                            })
                            .setNegativeButton("No",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    // if this button is clicked, just close
                                    // the dialog box and do nothing
                                    dialog.cancel();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }



        });


        next_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Implememnt logic
                Log.d("thingy","Hello");
                mah_grid.update_cells();//put function that checks logic
                mah_grid.invalidate();
            }
        });



    }



}